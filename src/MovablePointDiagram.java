import dev.*;
public class MovablePointDiagram {
    public static void main(String[] args) throws Exception {
        Cat cat = new Cat("Maria Ozawa");
        Dog dog = new Dog("Fukada");
        BigDog bigdog = new BigDog("Tokuda");
        cat.greets();
        dog.greets();
        dog.greets(new Dog("Emi"));           //chó khác có học ngoại ngữ
        bigdog.greets();                          // big chó cũ  
        bigdog.greets(new BigDog("Lao Ái"));//big chó mới keu kiểu khác

    }
}
