package dev;

public class BigDog extends Dog{

    public BigDog(String name) {
        super(name);
    }
    public void greets(){
        System.out.println("Wooow");
    }
    public void greets(Dog target){
        System.out.println("Woooooow");
    }
    public void greets(BigDog target){
        System.out.println("Wooooooooow");
    }
}
